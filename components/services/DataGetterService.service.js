(function() {
  "use strict";

  angular
    .module("boilerplate")
    .factory("DataGetterService", [
      "$window",
      "$q",
      "$log",
      "QueryService",
      "EventService",
      DataGetterService
    ]);

  function DataGetterService($window, $q, $log, QueryService, EventService) {
    return {
      getConceptGraphData: getConceptGraphData,
      getWebGraphData: getWebGraphData
    };

    ////////////////// definition

    function getConceptGraphData(query_id) {
      EventService.broadcast(
        EventService.events.API_REQUEST_START,
        EventService.api_calls.CONCEPT_CHART
      );
      let deferred = $q.defer();
      let final_data;
      // the first get
      QueryService.query("GET", "concept_graph/" + query_id + "/0", {}, {}).then(function(
        response
      ) {
        if (response.status === 200) {
          $log.info("GET /concept_graph/" + query_id + " success");
          // check if only one page we have
          if (response.pages === 1) {
            final_data = response.data;
            deferred.resolve(final_data);
          } else
            async.waterfall(
              prepareWaterfall(
                "concept_graph/" + query_id,
                "data",
                response.data,
                query_id,
                response.data.pages
              ),
              (err, url, result, query_id, page) => {
                if (err) {
                  $log.error("Waterfall GET /concept_graph/" + query_id + "/" + err + " error");
                  deferred.reject();
                } else deferred.resolve(result);
              }
            );
        } else {
          $log.error("GET /concept_graph/" + query_id + " error");
          deferred.reject();
        }
      });

      return deferred.promise;
    }

    function getWebGraphData(query_id) {
      EventService.broadcast(
        EventService.events.API_REQUEST_START,
        EventService.api_calls.WEB_GRAPH
      );

      let deferred = $q.defer();
      let final_data = { nodes: [], links: [] };

      let getNodes = query_id => {
        // the first get
        QueryService.query("GET", "web_graph/" + query_id + "/nodes/0", {}, {}).then(function(
          response
        ) {
          if (response.status === 200) {
            $log.info("GET /web_graph/" + query_id + "/nodes/0 success");
            // check if only one page we have
            if (response.pages === 1) {
              final_data.nodes = response.data;
              getLinks(query_id);
            } else
              async.waterfall(
                prepareWaterfall(
                  "web_graph/" + query_id + "/nodes",
                  "nodes",
                  response.data,
                  query_id,
                  response.data.pages
                ),
                (err, url, result, query_id, page) => {
                  if (err) {
                    $log.error("Waterfall GET /web_graph/" + query_id + "/nodes" + err + " error");
                    deferred.reject();
                  } else {
                    final_data = result;
                    getLinks(query_id);
                  }
                }
              );
          } else {
            $log.error("GET /concept_graph/" + query_id + " error");
            deferred.reject();
          }
        });
      };

      let getLinks = query_id => {
        QueryService.query("GET", "web_graph/" + query_id + "/links/0", {}, {}).then(function(
          response
        ) {
          if (response.status === 200) {
            $log.info("GET /web_graph/" + query_id + " success");
            // check if only one page we have
            if (response.pages === 1) {
              final_data.nodes = response.data;
              getLinks();
            } else
              async.waterfall(
                prepareWaterfall(
                  "web_graph/" + query_id + "/links",
                  "links",
                  response.data,
                  query_id,
                  response.data.pages
                ),
                (err, url, result, query_id, page) => {
                  if (err) {
                    $log.error("Waterfall GET /web_graph/" + query_id + "/" + err + " error");
                    deferred.reject();
                  } else {
                    final_data.links = result.links;
                    deferred.resolve(final_data);
                  }
                }
              );
          } else {
            $log.error("GET /concept_graph/" + query_id + " error");
            deferred.reject();
          }
        });
      };

      getNodes(query_id);
      return deferred.promise;
    }

    // not exported
    function prepareWaterfall(url, obj_key, data, query_id, pages) {
      let fn = (url, data_fn, query_id, page, callback) => {
        QueryService.query("GET", url + "/" + page, {}, {}).then(response => {
          if (response.status === 200) {
            $log.info("GET " + url + "/" + page + " success");
            data_fn[obj_key] = data_fn[obj_key].concat(response.data[obj_key]);
            callback(null, url, data_fn, query_id, page + 1);
          } else callback(page);
        });
      };

      let fns = [];
      fns.push(callback => {
        callback(null, url, data, query_id, 1);
      });

      for (let i = 1; i < pages; i++) fns.push(fn);
      return fns;
    }
  }
})();
