(function () {
  "use strict";

  angular
    .module("boilerplate")
    .factory("GlobalVariablesService", ["EventService", GlobalVariablesService]);

  function GlobalVariablesService(EventService) {
    let query_id = undefined;
    let selected_indices_radar = [];

    return {
      setQueryId: setQueryId,
      getQueryId: getQueryId,
      addIndexForRadar: addIndexForRadar,
      removeIndexForRadar: removeIndexForRadar,
      getIndicesForRadar: getIndicesForRadar,
      getIndexInArrayForRadar: getIndexInArrayForRadar,
      getColorForPageForRadar: getColorForPageForRadar,
      clearSelectedForRadar: clearSelectedForRadar,
      getColorScale: getColorScale,
      prepareChartArea: prepareChartArea
    };

    ////////////////// definitions
    function setQueryId(hash) {
      query_id = hash;
    }
    function getQueryId() {
      return query_id;
    }

    /**
     * Added an index in the results of a page
     * @param {int} page_result_index
     */
    function addIndexForRadar(page_result_index) {
      let index = selected_indices_radar.findIndex(e => e == page_result_index);
      if (index < 0) {
        selected_indices_radar.push(parseInt(page_result_index));
        selected_indices_radar.sort((a, b) => a - b);
        EventService.broadcast(EventService.events.PAGE_SELECTED_FOR_RADAR, page_result_index);
      }
    }

    function removeIndexForRadar(page_result_index) {
      let index = selected_indices_radar.findIndex(e => e == page_result_index);
      if (index >= 0) {
        selected_indices_radar.splice(index, 1);
        EventService.broadcast(EventService.events.PAGE_DESELECTED_FOR_RADAR, page_result_index);
      }
    }

    /**
     * Translate a result index to an index in the selected_indices_radar variable
     * @param {int} page_result_index
     */
    function getIndexInArrayForRadar(page_result_index) {
      return selected_indices_radar.findIndex(e => e == page_result_index);
    }

    function getIndicesForRadar() {
      return selected_indices_radar;
    }

    /**
     * Clear all selected index
     */
    function clearSelectedForRadar() {
      selected_indices_radar = [];
      EventService.broadcast(EventService.events.CLEAR_SELECTED_FOR_RADAR);
    }

    function getColorForPageForRadar(page_index) {
      return getColorScale(selected_indices_radar.length)(
        selected_indices_radar.findIndex(e => e == page_index)
      );
    }

    /**
     * Get the general color scale used in every chart
     * @param {int} range
     */
    function getColorScale(range) {
      return d3.scaleOrdinal(d3.schemeCategory10).domain(d3.range(range));
    }

    /**
     * Prepare the passed chart area. For example for initializing the label behavior
     * @param {*} chart_area
     */
    function prepareChartArea(chart_area) {
      let div = d3.select(chart_area);
      div.on("mouseenter", d => {
        div.select(".chart-label").transition().duration(500)
          .ease(d3.easeLinear).style("opacity", "0").transition().style("display", "none")
      })
      d3.select(chart_area).on("mouseleave", d => {
        div.select(".chart-label").transition().duration(500)
          .ease(d3.easeLinear).style("opacity", "1").style("display", "block")
      })
    }
  }
})();
