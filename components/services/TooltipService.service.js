(function () {
  "use strict";

  angular
    .module("boilerplate")
    .factory("TooltipService", ["$window", "CacheService", TooltipService]);

  function TooltipService($window, CacheService) {
    let tooltip_dom = document.querySelector(".page-tooltip");
    let tooltip = angular.element(tooltip_dom);
    let tooltip_title_dom = tooltip_dom.querySelector("h1");
    let tooltip_description_dom = tooltip_dom.querySelector("p.content");
    let tooltip_url_dom = tooltip_dom.querySelector("a");
    let tooltip_result_num_dom = tooltip_dom.querySelector("h2 span.result-num");
    let tooltip_concept_num_dom = tooltip_dom.querySelector("h2 span.concept-num");

    let xCoord = 0;
    let yCoord = 0;
    let loading = false;

    return {
      showTooltip: showTooltip,
      hideTooltip: hideTooltip,
      getTooltipHeight: getTooltipHeight,
      getTooltipWidth: getTooltipWidth,
      setPosition: setPosition,
      setLoading: setLoading,
      setTitle: setTitle,
      setContent: setContent,
      setColor: setColor,
      setLink: setLink,
      setResultNum: setResultNum,
      setConceptNum: setConceptNum,
      show: show,
      hide: hide
    };

    ////////////////// definition

    /**
     * Completely manage the showing with loading of the content
     * @param {Stirng} hash
     * @param {Float} x
     * @param {Float} y
     * @param {String} color
     */
    function showTooltip(hash, x, y, color, result_num, concept_num) {
      setPosition(x, y);
      setColor(color);
      setLoading(true);
      show();
      CacheService.getDocumentFromHash(hash).then(
        document => {
          if (document != null || document != undefined) {
            setTitle(document.title);
            setContent(document.excerpt);
            setLink(document.url, document.url);
            setResultNum(result_num);
            setConceptNum(concept_num);
            setLoading(false);
          }
        },
        reason => {
          hide();
        }
      );
    }

    /**
     * Hide the tooltip
     */
    function hideTooltip() {
      hide();
    }

    function getTooltipHeight() {
      return tooltip_dom.offsetHeight;
    }

    function getTooltipWidth() {
      return tooltip_dom.offsetWidth;
    }

    //// builder functions

    function setTitle(title) {
      tooltip_title_dom.innerText = title;
      return this;
    }

    function setContent(content) {
      tooltip_description_dom.innerText = content;
      return this;
    }

    function setLink(url, placeholder) {
      tooltip_url_dom.href = url;
      tooltip_url_dom.innerText = placeholder;
      return this;
    }

    function setResultNum(result_num) {
      if (result_num === null || result_num === undefined) tooltip_result_num_dom.innerText = "N/A";
      else tooltip_result_num_dom.innerText = result_num;
      return this;
    }

    function setConceptNum(concept_num) {
      if (concept_num === null || concept_num === undefined)
        tooltip_concept_num_dom.innerText = "N/A";
      else tooltip_concept_num_dom.innerText = concept_num;
      return this;
    }

    function setPosition(x, y) {
      xCoord = x;
      yCoord = y;
      tooltip.css("left", xCoord + "px");
      tooltip.css("top", yCoord + "px");
      return this;
    }

    function setLoading(b) {
      loading = b;
      if (b) tooltip.addClass("loading");
      else tooltip.removeClass("loading");
      fixPosition();
      return this;
    }

    function setColor(hex) {
      tooltip.css("border-top", "4px solid " + hex);
      tooltip.css("border-bottom", "2px solid " + hex);
      return this;
    }

    function show() {
      tooltip.removeClass("hidden");
      return this;
    }

    function hide() {
      tooltip.addClass("hidden");
      return this;
    }

    /// not exported
    function fixPosition() {
      if (yCoord + getTooltipHeight() > $window.pageYOffset + $window.innerHeight) {
        yCoord -= Math.abs(
          $window.pageYOffset + $window.innerHeight - (yCoord + getTooltipHeight())
        );
      }
      if (xCoord + getTooltipWidth() > $window.pageXOffset + $window.innerWidth) {
        xCoord -= getTooltipWidth();
      }
      setPosition(xCoord, yCoord);
    }
  }
})();
