(function() {
  "use strict";

  angular
    .module("boilerplate")
    .factory("CacheService", [
      "$window",
      "$q",
      "$log",
      "QueryService",
      "LocalStorage",
      CacheService
    ]);

  function CacheService($window, $q, $log, QueryService, LocalStorage) {
    return {
      getDocumentFromHash: getDocumentFromHash
    };

    ////////////////// definition

    /**
     * Return the information about a page from the its url hash. If it is not available in the
     * local storage then it will fallback to the get http request. This method always returns a
     * promise.
     * @param {*} hash
     */
    function getDocumentFromHash(hash) {
      let deferred = $q.defer();
      let page = LocalStorage.get("document-" + hash);
      // if page is not found in the local storage then make the request
      if (!page) {
        QueryService.query("GET", "document/" + hash, {}, {}).then(function(response) {
          if (response.status === 200) {
            $log.info("GET /document/" + hash + " success");
            // update the local storage
            LocalStorage.set("document-" + hash, response.data);
            page = response.data;
            deferred.resolve(page);
          } else {
            $log.error("GET /document/" + hash + " error");
          }
        });
      } else deferred.resolve(page);
      return deferred.promise;
    }
  }
})();
