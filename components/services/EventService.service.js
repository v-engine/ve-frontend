(function() {
  "use strict";

  angular
    .module("boilerplate")
    .factory("EventService", ["$rootScope", "$window", "$log", EventService]);

  function EventService($rootScope, $window, $log) {
    let events = {
      REMOVE_CLUSTER: "REMOVE_CLUSTER",
      REMOVE_PAGES: "REMOVE_PAGES",
      HOVER_ENTER: "HOVER_ENTER",
      HOVER_LEAVE: "HOVER_LEAVE",
      SELECTED_PAGES: "SELECTED_PAGES",
      DESELECTED_PAGES: "DESELECTED_PAGES",
      RESIZE_END: "RESIZE_END",
      PAGE_SELECTED_FOR_RADAR: "PAGE_SELECTED_FOR_RADAR",
      PAGE_DESELECTED_FOR_RADAR: "PAGE_DESELECTED_FOR_RADAR",
      ClEAR_SELECTED_FOR_RADAR: "CLEAR_SELECTED_FOR_RADAR",
      // api requests events
      API_REQUEST_START: "API_REQUEST_START",
      API_REQUEST_END: "API_REQUEST_END"
    };

    let api_calls = {
      QUERY: "QUERY_API",
      WEB_GRAPH: "WEB_GRAPH_API",
      CONCEPT_CHART: "CONCEPT_CHART_API",
      PCA_SCATTER_CHART: "PCA_SCATTER_CHART_API",
      TSNE_SCATTER_CHART: "TSNE_SCATTER_CHART_API",
      REMOVE_CLUSTER: "REMOVE_CLUSTER"
    };

    var timeout;
    // resize event broadcast
    angular.element($window).on("resize", function() {
      clearTimeout(timeout);
      timeout = setTimeout(d => {
        broadcast(events.RESIZE_END);
      }, 600);
    });

    return {
      broadcast: broadcast,
      events: events,
      api_calls: api_calls
    };

    ///////// definitions
    function broadcast(event, payload) {
      $rootScope.$broadcast(event, payload);
    }
  }
})();
