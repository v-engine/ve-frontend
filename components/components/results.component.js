(function() {
  "use strict";
  /**
   * Search component
   * @ngdoc  Component
   *
   * @example
   * <search></search>
   *
   */
  angular.module("boilerplate").component("results", {
    transclude: true,
    replace: true,
    templateUrl: "components/components/results.html",
    bindings: {
      data: "<", // define the one-way binding (this <-- outside)
      radarChartData: "<",
      conceptsChartData: "<"
    },
    controller: [
      "$log",
      "$scope",
      "GlobalVariablesService",
      "EventService",
      function ResultsController($log, $scope, GlobalVariablesService, EventService) {
        let ctrl = this;
        let CTRL_NAME = "ResultsController";
        /// variables
        let results_component = angular.element(document.querySelector(".results-component"));
        let results_area = document.querySelector(".results-component .results");
        let loading_events = [EventService.api_calls.QUERY, EventService.api_calls.REMOVE_CLUSTER];
        /// listeners
        $scope.$watch("$ctrl.data", function(v) {
          if (v !== undefined && typeof v === "object" && v.length != 0) {
            $log.debug(CTRL_NAME + ": results data changed!");
            ctrl.updateUi(true);
            setLoading(results_area, false);
          }
        });
        $scope.$on(EventService.events.API_REQUEST_START, (event, data) => {
          if (loading_events.indexOf(data) >= 0) {
            setLoading(results_area, true);
            $log.debug(CTRL_NAME + ": Received " + event.name);
          }
        });

        /// functions
        /**
         * Update the ui according to a boolean show variable
         * @param {*} show
         */
        ctrl.updateUi = show => {
          show ? results_component.removeClass("hidden") : results_component.addClass("hidden");
        };

        let setLoading = (area, b) => {
          if (b) area.classList.add("loading");
          else area.classList.remove("loading");
        };

        $scope.clickedResult = ($event, item, index) => {
          // ignore click if we want to open the page
          if ($event.target.nodeName === "A") return;
          let e = $event.currentTarget;
          let updateAllColors = _ => {
            let selected_results = document.querySelectorAll(
              "section.results .inner .entry.selected"
            );
            selected_results.forEach((e, i) => {
              e.style["border-left-color"] = GlobalVariablesService.getColorForPageForRadar(
                e.classList[1].split("-")[1]
              );
            });
          };
          if (e.classList.contains("selected")) {
            GlobalVariablesService.removeIndexForRadar(index);
            // update all colors
            updateAllColors();
            e.classList.remove("selected");
          } else {
            //e.style["border-left-color"] = GlobalVariablesService.getColorForPageForRadar(index);
            GlobalVariablesService.addIndexForRadar(index);
            e.classList.add("selected");
            updateAllColors();
          }
        };
      }
    ]
  });
})();
