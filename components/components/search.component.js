(function() {
  "use strict";
  /**
   * Search component
   * @ngdoc  Component
   *
   * @example
   * <search></search>
   *
   */
  angular.module("boilerplate").component("search", {
    transclude: true,
    replace: true,
    templateUrl: "components/components/search.html",
    bindings: {
      query: "=",
      results: "<"
    },
    controller: [
      "$log",
      "$scope",
      "$route",
      function SearchComponentController($log, $scope, $route) {
        let CTRL_NAME = "SearchComponentController";
        let ctrl = this;
        let search_bar = document.querySelector("section.search");
        let button_search = angular.element(document.querySelector("button"))[0];

        /// listeners
        /**
         * Listen for the query to change. This is useful when pages with /q/
         * and query is specified
         */
        $scope.$watch("$ctrl.query", function(v) {
          if (v !== undefined && v !== "" && $route.current.controller === "SearchController") {
            $log.debug(CTRL_NAME + ": $ctrl.query = " + v);
            $scope.query_input = v;
            button_search.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>';
            setTimeout(d => {
              setCollapse(true);
            }, 200);
          }
        });

        /**
         * Listen for result here, so we know when they are ready
         */
        $scope.$watch("$ctrl.results", function(v) {
          if (v !== undefined && (typeof v === "object" && v.length > 0)) {
            $log.debug(CTRL_NAME + ': $watch("$ctrl.results"');
            // only update the ui when results are ready
            //setCollapse(true);
            button_search.innerText = "Search";
          }
        });

        /// functions
        /**
         * Main handler of the search function
         */
        ctrl.search = () => {
          // set the bound variable only if there is a query
          if ($scope.query_input !== undefined && $scope.query_input !== "") {
            ctrl.query = $scope.query_input;
            //ctrl.updateUi(true);
            $log.debug(CTRL_NAME + ": Query is " + ctrl.query);
          }
        };

        /**
         * Update the ui specifying if it should be collapsed or not
         * @param {*} collapsed
         */
        let setCollapse = collapsed => {
          collapsed
            ? search_bar.classList.add("collapsed")
            : search_bar.classList.remove("collapsed");
        };
      }
    ]
  });
})();
