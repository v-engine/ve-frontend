(function () {
  "use strict";
  /**
   * Charts component
   * @ngdoc  Component
   *
   * @example
   * <search></search>
   *
   */
  angular.module("boilerplate").component("charts", {
    transclude: true,
    templateUrl: "components/components/charts.html",
    bindings: {
      webGraphChartData: "<",
      conceptsChartData: "<",
      pcaScatterChartData: "<",
      tsneScatterChartData: "<",
      query: "<"
    },
    controller: [
      "$scope",
      "$log",
      "EventService",
      function ChartsController($scope, $log, EventService) {
        /// variables
        let ctrl = this;
        let shown = false;
        let charts_area = angular.element(document.querySelector("section.charts"));

        /// listeners
        /**
         * Listen here for the web graph chart data
         */
        $scope.$watch("$ctrl.webGraphChartData", function (v) {
          if (v !== undefined) ctrl.showContent(true);
        });

        /**
         * Listen here for the concept chart data
         */
        $scope.$watch("$ctrl.conceptsChartData", function (v) {
          if (v !== undefined) ctrl.showContent(true);
        });

        /**
         * Listen here for the concept chart data
         */
        $scope.$watch("$ctrl.query", function (v) {
          if (v !== undefined) ctrl.showPanel(true, true);
        });

        /// functions
        ctrl.showPanel = (show, loading) => {
          if ((show && shown) || (!show && !shown)) return;
          shown = show;
          show ? charts_area.removeClass("hidden") : charts_area.addClass("hidden");
          loading ? charts_area.addClass("loading") : charts_area.removeClass("loading");
        };

        ctrl.showContent = show => {
          show ? charts_area.removeClass("loading") : charts_area.addClass("loading");
        };

        ctrl.dataReady = () => {
          return ctrl.webGraphChartData !== undefined && ctrl.conceptsChartData != undefined;
        };
      }
    ]
  });
})();
