(function() {
  "use strict";

  /**
   * D3 example
   * @ngdoc  Directive
   *
   * @example
   * <d3-test></d3-test>
   *
   */
  angular
    .module("boilerplate")
    .directive("webGraphChart", [
      "$log",
      "$location",
      "EventService",
      "TooltipService",
      "CacheService",
      "GlobalVariablesService",
      WebGraphChart
    ]);

  function WebGraphChart(
    $log,
    $location,
    EventService,
    TooltipService,
    CacheService,
    GlobalVariablesService
  ) {
    let CHART_NAME = "WebGraphChart";
    // Definition of directive
    let directiveDefinitionObject = {
      restrict: "E",
      replace: true,
      templateUrl: "components/directives/web_graph_chart.html",
      scope: { name: "@name", data: "=chartData", conceptData: "=conceptData" },
      controller: [
        "$scope",
        function WebChartGraphController($scope) {
          $scope.currently_hover = undefined;
        }
      ],
      link: function(scope, element, attrs, ctrl) {
        /// variables
        let chart_area = element[0];
        GlobalVariablesService.prepareChartArea(chart_area);
        let loading_events = [
          EventService.api_calls.WEB_GRAPH,
          EventService.api_calls.QUERY,
          EventService.api_calls.REMOVE_CLUSTER,
          EventService.api_calls.REMOVE_PAGES
        ];
        /// listeners
        scope.$on(EventService.events.HOVER_ENTER, (event, data) => {
          if (data !== scope.currently_hover) hoverEnter(scope, chart_area, data);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.HOVER_LEAVE, (event, data) => {
          hoverLeave(scope, chart_area, data);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.SELECTED_PAGES, (event, data) => {
          selectPages(scope, chart_area, data);
          scope.brush_mode = data.length === 0 ? false : true;
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.RESIZE_END, (event, data) => {
          updateGraph(scope, chart_area);
          updateColors(scope, chart_area);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.API_REQUEST_START, (event, data) => {
          if (loading_events.indexOf(data) >= 0) {
            setLoading(chart_area, true);
            $log.debug(CHART_NAME + ": Received " + event.name);
          }
        });
        // watch the data variable and update the graph when it changes
        scope.$watch("data", function(new_value) {
          if (new_value != undefined) updateGraph(scope, chart_area);
        });
        scope.$watch("conceptData", function(new_value) {
          if (new_value != undefined) updateColors(scope, chart_area);
        });
      }
    };

    /**
     * Update graph routine
     * From https://bl.ocks.org/mbostock/4062045
     * @param {*} scope
     */
    let updateGraph = function(scope, chart_area) {
      $log.debug(CHART_NAME + ": updateGraph() called, data is " + scope.data);
      // check if data is present
      if (scope.data === undefined) return;

      let width = chart_area.clientWidth,
        height = chart_area.clientHeight,
        circles_radius_min = 3,
        circles_radius_max = 10;

      let div = d3.select(chart_area);
      div.selectAll("svg").remove();
      let svg = div.append("svg");

      // Zoom section
      var transform = d3.zoomIdentity;
      svg.call(
        d3
          .zoom()
          .scaleExtent([1 / 2, 8])
          .on("zoom", zoomed)
      );
      function zoomed() {
        node.attr("transform", d3.event.transform);
        link.attr("transform", d3.event.transform);
      }

      let radius_scale = d3
        .scaleLinear()
        .domain([scope.data.weights.min_weight, scope.data.weights.max_weight])
        .range([circles_radius_min, circles_radius_max]);

      var simulation = d3
        .forceSimulation()
        .force(
          "link",
          d3
            .forceLink()
            .id(d => d.url_hash)
            .strength(0.1)
        )
        .force(
          "charge",
          d3
            .forceManyBody()
            .strength(-60)
            .distanceMax(150)
        )
        .force("center", d3.forceCenter(width / 2, height / 2));

      var link = svg
        .append("g")
        .attr("class", "links")
        .selectAll("line")
        .data(scope.data.links)
        .enter()
        .append("line")
        .attr("class", d => {
          return "from-" + d.source + " to-" + d.target;
        })
        .attr("stroke-width", function(d) {
          return Math.sqrt(d.value);
        });

      var node = svg
        .append("g")
        .attr("class", "nodes")
        .selectAll("circle")
        .data(scope.data.nodes)
        .enter()
        .append("circle")
        .attr("id", function(d) {
          return "node-" + d.url_hash;
        })
        .attr("r", d => radius_scale(d.weight))
        .call(
          d3
            .drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended)
        )
        .on("mouseenter", mouseenter)
        .on("mouseleave", mouseleave)
        .on("click", (d, i) => {
          setLoading(chart_area, true);
          CacheService.getDocumentFromHash(d.url_hash).then(page => {
            setLoading(chart_area, false);
            window.location.href = page.url;
          });
        });

      node.append("title").text(function(d) {
        return d.url;
      });

      simulation.nodes(scope.data.nodes).on("tick", ticked);

      simulation.force("link").links(scope.data.links);

      function ticked() {
        link
          .attr("x1", function(d) {
            return d.source.x;
          })
          .attr("y1", function(d) {
            return d.source.y;
          })
          .attr("x2", function(d) {
            return d.target.x;
          })
          .attr("y2", function(d) {
            return d.target.y;
          });

        node
          .attr("cx", function(d) {
            return d.x;
          })
          .attr("cy", function(d) {
            return d.y;
          });
      }

      function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }

      function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      }

      function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }

      function mouseenter(d) {
        if (!scope.brush_mode) {
          EventService.broadcast(EventService.events.HOVER_ENTER, d.url_hash);
          hoverEnter(scope, chart_area, d.url_hash);
        }
        let el = findElementInConceptDataByHash(scope, d.url_hash);
        let mouse_coords = d3.mouse(this);
        TooltipService.showTooltip(
          d.url_hash,
          mouse_coords[0] + radius_scale(d.weight) * 3,
          mouse_coords[1],
          GlobalVariablesService.getColorScale(scope.conceptData.n_clusters)(el.cluster_index),
          el.index + 1,
          el.cluster_index
        );
      }

      function mouseleave(d) {
        if (!scope.brush_mode) {
          EventService.broadcast(EventService.events.HOVER_LEAVE, d.url_hash);
          hoverLeave(scope, chart_area, d.url_hash);
        }
        TooltipService.hideTooltip();
      }

      setLoading(chart_area, false);
    };

    /**
     * Update the the nodes colors when cluster data is ready
     */
    let updateColors = function(scope, chart_area) {
      $log.debug(CHART_NAME + ": updateColors() called, data is " + scope.conceptData);
      // check if data is present
      if (scope.data === undefined) return;

      let svg = d3.select(chart_area).select("svg");
      let color_scale = GlobalVariablesService.getColorScale(scope.conceptData.n_clusters);

      svg
        .select("g.nodes")
        .selectAll("circle")
        .transition()
        .duration(500)
        .ease(d3.easeLinear)
        .style("fill", function(d) {
          return color_scale(findElementInConceptDataByHash(scope, d.url_hash).cluster_index);
        });
    };

    let findElementInConceptDataByHash = (scope, hash) => {
      return scope.conceptData.data[
        scope.conceptData.data.findIndex(item => {
          return item.url_hash === hash;
        })
      ];
    };

    let hoverEnter = (scope, chart_area, hash) => {
      scope.currently_hover = hash;
      d3.select(chart_area)
        .select("svg")
        .select("g.nodes")
        .selectAll("circle")
        .classed("dimmed", el => {
          return el.url_hash !== hash;
        });
      d3.select(chart_area)
        .select("svg")
        .select("g.links")
        .selectAll("line")
        .classed("highlight", el => {
          return el.source.url_hash === hash || el.target.url_hash === hash;
        })
        .classed("dimmed", el => {
          return !(el.source.url_hash === hash || el.target.url_hash === hash);
        });
    };

    let hoverLeave = (scope, chart_area, hash) => {
      scope.currently_hover = undefined;
      d3.select(chart_area)
        .select("svg")
        .select("g.nodes")
        .selectAll("circle")
        .classed("dimmed", false);
      d3.select(chart_area)
        .select("svg")
        .select("g.links")
        .selectAll("line")
        .classed("highlight", false)
        .classed("dimmed", false);
    };

    let setLoading = (chart_area, b) => {
      if (b) chart_area.classList.add("loading");
      else chart_area.classList.remove("loading");
    };

    let selectPages = (scope, chart_area, pages_hashes) => {
      let svg = d3.select(chart_area).select("svg");
      let all_dots = svg.select("g.nodes").selectAll("circle");
      let all_links = svg.select("g.links").selectAll("line");
      // deselect all pages
      if (pages_hashes === null || pages_hashes.length === 0) {
        all_dots.classed("dimmed", false);
        all_links.classed("dimmed", false);
        all_links.classed("highlight", false);
        return;
      }
      all_dots.classed("dimmed", true);
      all_links.classed("dimmed", true);
      pages_hashes.forEach((hash, i) => {
        // nodes
        svg.select("#node-" + hash).classed("dimmed", false);
        // links
        svg
          .select("g.links")
          .selectAll("line.from-" + hash)
          .classed("dimmed", false)
          .classed("highlight", true);
        svg
          .select("g.links")
          .selectAll("line.to-" + hash)
          .classed("dimmed", false)
          .classed("highlight", true);
      });
    };

    return directiveDefinitionObject;
  }
})();
