(function() {
  "use strict";

  /**
   * D3 example
   * @ngdoc  Directive
   *
   * @example
   * <d3-test></d3-test>
   *
   */
  angular
    .module("boilerplate")
    .directive("gridChart", [
      "$log",
      "EventService",
      "CacheService",
      "GlobalVariablesService",
      "TooltipService",
      GridChart
    ]);

  function GridChart($log, EventService, CacheService, GlobalVariablesService, TooltipService) {
    let CHART_NAME = "GridChart";
    // Definition of directive
    let directiveDefinitionObject = {
      restrict: "E",
      replace: true,
      templateUrl: "components/directives/grid_chart.html",
      scope: { name: "@name", webGraphData: "=webGraphData", conceptData: "=conceptData" },
      controller: [
        "$scope",
        function GridChartController($scope) {
          $scope.currently_hover = undefined;
          $scope.brush_mode = false;
          /// listeners
          /**
           * Listen here the remove cluster event
           */
          $scope.$on("EventService.REMOVE_CLUSTER_FROM_CHART_COMPONENT", (event, data) => {
            $log.debug(
              "WebChartGraphController: Received EventService.REMOVE_CLUSTER_FROM_CHART_COMPONENT"
            );
          });
        }
      ],
      link: function(scope, element, attrs) {
        /// variables
        let chart_area = element[0];
        GlobalVariablesService.prepareChartArea(chart_area);
        let loading_events = [
          EventService.api_calls.QUERY,
          EventService.api_calls.CONCEPT_CHART,
          EventService.api_calls.REMOVE_CLUSTER,
          EventService.api_calls.REMOVE_PAGES
        ];
        /// listeners
        scope.$on(EventService.events.HOVER_ENTER, (event, data) => {
          if (data !== scope.currently_hover) hoverEnter(scope, chart_area, data);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.HOVER_LEAVE, (event, data) => {
          hoverLeave(scope, chart_area, data);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.SELECTED_PAGES, (event, data) => {
          selectPages(scope, chart_area, data);
          scope.brush_mode = data.length === 0 ? false : true;
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.RESIZE_END, (event, data) => {
          updateGraph(scope, chart_area);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.API_REQUEST_START, (event, data) => {
          if (loading_events.indexOf(data) >= 0) {
            setLoading(chart_area, true);
            $log.debug(CHART_NAME + ": Received " + event.name);
          }
        });
        // watch the data variable and update the graph when it changes
        scope.$watch("conceptData", function(new_value) {
          if (new_value != undefined) updateGraph(scope, chart_area);
        });
      }
    };

    /**
     * Update graph routine
     * From https://bl.ocks.org/mbostock/4062045
     * @param {*} scope
     */
    let updateGraph = function(scope, chart_area) {
      $log.debug(CHART_NAME + ": Update graph called, data is " + scope.conceptData);
      // check if data is present
      if (scope.conceptData === undefined) return;

      let width = chart_area.clientWidth,
        height = chart_area.clientHeight;

      var square_num = scope.conceptData.data.length;
      var margin_vertical = 5,
        margin_horizontal = 5,
        square_margin = 3;

      var w = width - margin_horizontal * 2,
        h = height - margin_vertical * 2,
        square_size = Math.sqrt((w * h) / square_num);

      var color_scale = GlobalVariablesService.getColorScale(scope.conceptData.n_clusters);

      d3.select(chart_area)
        .selectAll("svg")
        .remove();

      // create the svg
      var svg = d3
        .select(chart_area)
        .append("svg")
        .attrs({
          width: w,
          height: h
        });

      // calculate number of rows and columns
      var squaresRow = Math.ceil(w / square_size);
      var squaresColumn = Math.ceil(h / square_size);
      // update the square size to fit the new values
      let new_row_size = w / squaresRow;
      let new_col_size = h / squaresColumn;
      let isFitting = (size, num, dimX, dimY) => size * num <= dimX && size * num <= dimY;
      square_size = isFitting(Math.max(new_row_size, new_col_size, w, h))
        ? Math.max(new_row_size, new_col_size, w, h)
        : Math.min(new_row_size, new_col_size, w, h);
      // check if we can fit more in the row
      //if (w - square_size * squaresRow > square_size)
      //  squaresRow += Math.floor((w - square_size * squaresRow) / square_size);
      // compute the fix to center the chart
      var horizontalFix = (w - squaresRow * square_size + square_margin) / 2;
      var verticalFix = (h - squaresColumn * square_size + square_margin) / 2;

      // loop over number of columns
      _.times(squaresColumn, function(n) {
        // create each set of rows
        let needed_squares =
          n === squaresColumn - 1 ? scope.conceptData.data.length - n * squaresRow : squaresRow;
        var rows = svg
          .selectAll("rect" + " .row-" + (n + 1))
          .data(
            d3.range(needed_squares).map(i => {
              let req_i = i + squaresRow * n;
              return {
                index: i,
                x: 0,
                y: 0,
                url_hash: scope.conceptData.data[req_i].url_hash,
                cluster_index: scope.conceptData.data[req_i].cluster_index
              };
            })
          )
          .enter()
          .append("rect")
          .attrs({
            class: function(d, i) {
              return (
                "square row-" +
                (n + 1) +
                " " +
                "col-" +
                (i + 1) +
                " square-" +
                scope.conceptData.data[i + squaresRow * n].url_hash
              );
            },
            id: function(d, i) {
              return "s-" + (n + 1) + (i + 1);
            },
            width: square_size - square_margin,
            height: square_size - square_margin,
            x: function(d, i) {
              if (i === 0) d.x = horizontalFix + margin_horizontal;
              else d.x = horizontalFix + margin_horizontal + i * square_size;
              return d.x;
            },
            y: (d, i) => {
              if (n === 0) d.y = verticalFix + margin_vertical;
              else d.y = verticalFix + margin_vertical + n * square_size;
              return d.y;
            },
            fill: function(d, i) {
              return color_scale(scope.conceptData.data[i + squaresRow * n].cluster_index);
            },
            rx: 1.5,
            ry: 1.5
            //stroke: "#FFF"
          })
          .on("click", (d, i) => {
            setLoading(chart_area, true);
            CacheService.getDocumentFromHash(d.url_hash).then(page => {
              setLoading(chart_area, false);
              window.location.href = page.url;
            });
          });

        rows.on("mouseenter", function(d, i) {
          if (!scope.brush_mode) {
            EventService.broadcast(EventService.events.HOVER_ENTER, d.url_hash);
            hoverEnter(scope, chart_area, d.url_hash);
          }
          TooltipService.showTooltip(
            d.url_hash,
            square_size + d.x,
            chart_area.offsetTop + d.y,
            color_scale(d.cluster_index),
            i + squaresRow * n + 1,
            d.cluster_index
          );
        });
        rows.on("mouseleave", function(d, i) {
          if (!scope.brush_mode) {
            EventService.broadcast(EventService.events.HOVER_LEAVE, d.url_hash);
            hoverLeave(scope, chart_area);
          }
          TooltipService.hideTooltip();
        });
      });
      setLoading(chart_area, false);
    };

    /**
     * Update the the nodes colors when cluster data is ready
     */
    let updateColors = function(scope, svg_dom, conceptData) {
      let svg = d3.select(svg_dom);
      let color_scale = d3
        .scaleOrdinal(d3.schemeCategory10)
        .domain(d3.range(conceptData.n_clusters));
      /*
      var node_new = svg
        .select("g.nodes")
        .selectAll("circle")
        .transition()
        .duration(500)
        .ease(d3.easeLinear)
        .style("fill", function(d) {
          return color_scale(
            conceptData.data[
              conceptData.data.findIndex(item => {
                return item.url_hash === d.url_hash;
              })
            ].cluster_index
          );
        });
        */
    };

    let hoverEnter = (scope, chart_area, hash) => {
      scope.currently_hover = hash;
      let svg = d3.select(chart_area).select("svg");
      svg.selectAll(".square").classed("dimmed", (d, i) => {
        return d.url_hash !== hash;
      });
    };

    let hoverLeave = (scope, chart_area) => {
      scope.currently_hover = undefined;
      d3.select(chart_area)
        .select("svg")
        .selectAll(".square")
        .classed("dimmed", false);
    };

    let setLoading = (chart_area, b) => {
      if (b) chart_area.classList.add("loading");
      else chart_area.classList.remove("loading");
    };

    let selectPages = (scope, chart_area, pages_hashes) => {
      if (pages_hashes === null || pages_hashes.length === 0) {
        d3.select(chart_area)
          .select("svg")
          .selectAll(".square")
          .classed("dimmed", false);
        return;
      }
      d3.select(chart_area)
        .select("svg")
        .selectAll(".square")
        .classed("dimmed", true);
      pages_hashes.forEach((hash, i) => {
        d3.select(chart_area)
          .select("svg")
          .selectAll(".square.square-" + hash)
          .classed("dimmed", false);
      });
    };

    return directiveDefinitionObject;
  }
})();
