(function() {
  "use strict";

  /**
   * Concept Chart
   * @ngdoc  Directive
   *
   * @example
   * <d3-test></d3-test>
   *
   */
  angular
    .module("boilerplate")
    .directive("conceptsChart", [
      "$log",
      "EventService",
      "CacheService",
      "GlobalVariablesService",
      "TooltipService",
      ConceptsChart
    ]);

  function ConceptsChart($log, EventService, CacheService, GlobalVariablesService, TooltipService) {
    let CHART_NAME = "ConceptsChart";
    // Definition of directive
    let directiveDefinitionObject = {
      restrict: "E",
      replace: true,
      templateUrl: "components/directives/concepts_chart.html",
      scope: { name: "@name", data: "=chartData" },
      controller: [
        "$scope",
        function ConceptsChartController($scope) {
          $scope.currently_hover = undefined;
          $scope.context_menu = [
            {
              title: "Query refinement"
            },
            {
              title: d => {
                return (
                  "Mark pages in concept #" + d.cluster_index + " as non-relevant and re-run search"
                );
              },
              action: function(e, d, i) {
                $scope.brush_mode = false;
                removeCluster($scope, e.cluster_index);
              }
            }
          ];
          $scope.circle_dimmed = false;
          $scope.opened_concepts = 0;
          $scope.brush_mode = false;
        }
      ],
      link: function(scope, element, attrs, ctrl) {
        //
        /// variables
        let chart_area = element[0];
        GlobalVariablesService.prepareChartArea(chart_area);
        let loading_events = [
          EventService.api_calls.CONCEPT_CHART,
          EventService.api_calls.QUERY,
          EventService.api_calls.REMOVE_CLUSTER,
          EventService.api_calls.REMOVE_PAGES
        ];
        /// listeners
        scope.$on(EventService.events.HOVER_ENTER, (event, data) => {
          if (data !== scope.currently_hover) hoverEnter(scope, chart_area, data);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.HOVER_LEAVE, (event, data) => {
          hoverLeave(scope, chart_area, data);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.SELECTED_PAGES, (event, data) => {
          selectPages(scope, chart_area, data);
          scope.brush_mode = data.length === 0 ? false : true;
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.RESIZE_END, (event, data) => {
          updateGraph(scope, chart_area);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.API_REQUEST_START, (event, data) => {
          if (loading_events.indexOf(data) >= 0) {
            setLoading(chart_area, true);
            $log.debug(CHART_NAME + ": Received " + event.name);
          }
        });
        // watch the data variable and update the graph when it changes
        scope.$watch("data", function(new_data) {
          if (
            new_data != undefined &&
            new_data.hasOwnProperty("data") &&
            new_data.data.length > 0
          ) {
            updateGraph(scope, chart_area);
          }
        });
      }
    };

    let updateGraph = (scope, chart_area) => {
      $log.debug(CHART_NAME + ": Update graph called , data is " + scope.data);
      // check if data is present
      if (scope.data === undefined) return;

      let radviz = radvizComponent(scope)
        .config({
          el: chart_area,
          height: chart_area.clientHeight,
          width: chart_area.clientWidth,
          margin: 50,
          dimensions: scope.data.dimensions.map((el, i) => i),
          useTooltip: true,
          colorAccessor: function(d) {
            return d.cluster_index;
          },
          colorScale: GlobalVariablesService.getColorScale(scope.data.n_clusters),
          drawLinks: true,
          zoomFactor: 1,
          dotRadius: 4,
          useRepulsion: false
        })
        .render(scope.data);
      setLoading(chart_area, false);
    };

    let utils = {
      merge: function(obj1, obj2) {
        for (var p in obj2) {
          if (obj2[p] && obj2[p].constructor == Object) {
            if (obj1[p]) {
              this.merge(obj1[p], obj2[p]);
              continue;
            }
          }
          obj1[p] = obj2[p];
        }
      },
      mergeAll: function() {
        var newObj = {};
        var objs = arguments;
        for (var i = 0; i < objs.length; i++) {
          this.merge(newObj, objs[i]);
        }
        return newObj;
      },
      htmlToNode: function(htmlString, parent) {
        while (parent.lastChild) {
          parent.removeChild(parent.lastChild);
        }
        return this.appendHtmlToNode(htmlString, parent);
      },
      appendHtmlToNode: function(htmlString, parent) {
        return parent.appendChild(
          document.importNode(
            new DOMParser().parseFromString(htmlString, "text/html").body.childNodes[0],
            true
          )
        );
      }
    };

    let radvizComponent = function(scope) {
      var config = {
        el: null,
        height: 400,
        width: 400,
        margin: 50,
        colorScale: d3.scaleOrdinal().range(["skyblue", "orange", "lime"]),
        colorAccessor: null,
        dimensions: [],
        drawLinks: true,
        zoomFactor: 1,
        dotRadius: 6,
        useRepulsion: false,
        useTooltip: true,
        tooltipFormatter: function(d) {
          return d;
        }
      };
      var events = d3.dispatch("panelEnter", "panelLeave", "dotEnter", "dotLeave");

      var render = function(data) {
        var thetaScale = d3
          .scaleLinear()
          .domain([0, config.dimensions.length])
          .range([0, Math.PI * 2]);

        let strength_scale = d3
          .scaleLinear()
          //  .domain([data.min_weight, data.max_weight])
          .range([0, 1]);

        var chartRadius = Math.min(config.height, config.width) / 2 - config.margin;
        var nodeCount = data.data.length;
        var panelHeight = config.height - config.margin * 2;
        var panelWidth = config.width - config.margin * 2;

        var dimensionNodes = config.dimensions.map(function(d, i) {
          var angle = thetaScale(i);
          var x = config.width / 2 + Math.cos(angle) * chartRadius * config.zoomFactor;
          var y = config.height / 2 + Math.sin(angle) * chartRadius * config.zoomFactor;
          return {
            index: nodeCount + i,
            x: x,
            y: y,
            fx: x,
            fy: y,
            name: d,
            cluster_index: i
          };
        });

        var linksData = [];
        data.data.forEach(function(d, i) {
          config.dimensions.forEach(function(dB, iB) {
            linksData.push({
              source: i, // from page node
              target: nodeCount + iB, // to concept node
              value: d["concept_weights"][iB],
              cluster_index: iB
            });
          });
        });

        var force = d3
          .forceSimulation(data.data.concat(dimensionNodes))
          .force(
            "link",
            d3.forceLink(linksData).strength(function(d) {
              if (isNaN(d.value)) {
                $log.error(
                  "Value for link " + d.source.index + " -> " + d.target.index + " is not a number"
                );
                return Math.random();
              }
              return strength_scale(d.value);
            })
          )
          .force(
            "charge",
            d3
              .forceManyBody()
              .strength(-100)
              .distanceMax(200)
          )
          .alpha(0.2)
          .force("center", d3.forceCenter(config.width / 2, config.height / 2));

        d3.select(config.el)
          .selectAll("svg")
          .remove();

        var svg = d3
          .select(config.el)
          .append("svg")
          .attrs({
            width: config.width,
            height: config.height
          });

        svg
          .append("rect")
          .classed("bg", true)
          .attrs({
            width: config.width,
            height: config.height
          });

        var root = svg.append("g");

        var panel = root
          .append("circle")
          .classed("panel", true)
          .attrs({
            r: chartRadius,
            cx: config.width / 2,
            cy: config.height / 2
          });

        if (config.useRepulsion) {
          root.on("mouseenter", function(d) {
            force
              .force(
                "charge",
                d3
                  .forceManyBody()
                  .strength(-100)
                  .distanceMax(200)
              )
              .alpha(0.2);
            //events.panelEnter();
          });
          root.on("mouseleave", function(d) {
            force
              .force(
                "charge",
                d3
                  .forceManyBody()
                  .strength(-50)
                  .distanceMax(10)
              )
              .restart();
            //events.panelLeave();
          });
        }

        if (config.drawLinks) {
          var links = root
            .selectAll(".link.chart-link")
            .data(linksData)
            .enter()
            .append("line")
            .attr("class", d => {
              return (
                "link chart-link from-" + d.source.url_hash + " cluster-" + d.source.cluster_index
              );
            })
            .style("stroke", d => {
              return config.colorScale(config.colorAccessor(d));
            });
        }

        var nodes = root
          .selectAll("circle.dot.chart-page")
          .data(data.data)
          .enter()
          .append("circle")
          .attr("class", function(d) {
            return "dot chart-page dot-" + d.url_hash;
          })
          .attrs({
            r: config.dotRadius,
            fill: function(d) {
              return config.colorScale(config.colorAccessor(d));
            }
          })
          .on("mouseenter", function(d) {
            // prevent the hovering logic if concept nodes are opened
            if (scope.opened_concepts > 0) return;
            // broadcast the event
            if (!scope.brush_mode) {
              EventService.broadcast(EventService.events.HOVER_ENTER, d.url_hash);
              hoverEnter(scope, config.el, d.url_hash, this);
            }
            let mouse_coords = d3.mouse(this);
            if (config.useTooltip) {
              TooltipService.showTooltip(
                d.url_hash,
                mouse_coords[0] + config.dotRadius * 3,
                mouse_coords[1],
                config.colorScale(config.colorAccessor(d)),
                d.index + 1,
                d.cluster_index
              );
            }
            //events.dotEnter(d);
          })
          .on("mouseleave", function(d) {
            // prevent the hovering logic if concept nodes are opened
            if (scope.opened_concepts > 0) return;
            // broadcast the event
            if (!scope.brush_mode) {
              EventService.broadcast(EventService.events.HOVER_LEAVE, d.url_hash);
              hoverLeave(scope, config.el, d.url_hash, this);
            }
            if (config.useTooltip) TooltipService.hideTooltip();
            //events.dotLeave(d);
          })
          .on("click", (d, i) => {
            setLoading(config.el, true);
            CacheService.getDocumentFromHash(d.url_hash).then(page => {
              setLoading(config.el, false);
              window.location.href = page.url;
            });
          });

        var labelNodes = root
          .selectAll("circle.label-node")
          .data(dimensionNodes)
          .enter()
          .append("circle")
          .classed("label-node", true)
          .style("display", "none")
          .attrs({
            cx: function(d) {
              return d.x;
            },
            cy: function(d) {
              return d.y;
            },
            r: 4
          })
          .style("fill", d => {
            return config.colorScale(config.colorAccessor(d));
          });

        var labels = root
          .selectAll("text.label")
          .data(dimensionNodes)
          .enter()
          .append("text")
          .attr("class", d => "label label-" + d.cluster_index)
          .attrs({
            x: function(d) {
              return d.x;
            },
            y: function(d) {
              return d.y;
            },
            "text-anchor": function(d) {
              if (d.x > config.width * 0.4 && d.x < config.width * 0.6) {
                return "middle";
              } else {
                return d.x > config.width / 2 ? "start" : "end";
              }
            },
            "dominant-baseline": function(d) {
              return d.y > config.height * 0.6 ? "hanging" : "auto";
            },
            dx: function(d) {
              return d.x > panelWidth / 2 ? "6px" : "-6px";
            },
            dy: function(d) {
              return d.y > panelHeight * 0.6 ? "10px" : "-10px";
            }
          })
          .text(function(d) {
            return d.name;
          });

        // nodes group and their force simulation
        createLabelNodesGroup(scope, config, root, chartRadius, thetaScale);

        force.on("tick", ticked);
        return this;

        /////// functions

        function ticked() {
          if (config.drawLinks) {
            links.attrs({
              x1: function(d) {
                return d.source.x;
              },
              y1: function(d) {
                return d.source.y;
              },
              x2: function(d) {
                return d.target.x;
              },
              y2: function(d) {
                return d.target.y;
              }
            });
          }
          nodes.attrs({
            cx: function(d) {
              return d.x;
            },
            cy: function(d) {
              return d.y;
            }
          });
        }
      };

      ////// main function continues here

      let setConfig = function(_config) {
        config = utils.mergeAll(config, _config);
        return this;
      };

      let exports = {
        config: setConfig,
        render: render
      };

      d3.rebind(exports, events, "on");
      return exports;
    };

    ////////////  functions

    let createLabelNodesGroup = (scope, config, root, chartRadius, thetaScale) => {
      let nodeCount = scope.data.data.length;
      let computeX = angle => config.width / 2 + Math.cos(angle) * chartRadius * config.zoomFactor;
      let computeY = angle => config.height / 2 + Math.sin(angle) * chartRadius * config.zoomFactor;

      let dim;
      let dimensions_pages_nodes = [];
      let dimensions_pages_links = [];
      let dimensions_pages_nodes_render = [];
      let dimensions_pages_links_render = [];
      let force = [];

      for (let dim_i = 0; dim_i < scope.data.dimensions.length; dim_i++) {
        dim = scope.data.dimensions[dim_i];
        dimensions_pages_nodes.push([]);
        dimensions_pages_links.push([]);
        // create the page nodes
        dim.forEach((page, page_i) => {
          dimensions_pages_nodes[dim_i].push({
            index: page_i,
            x: computeX(thetaScale(dim_i)),
            y: computeY(thetaScale(dim_i)),
            url_hash: page,
            cluster_index: dim_i
          });
        });
        // add fake a node if we have only one node
        if (dimensions_pages_nodes[dim_i].length === 1)
          dimensions_pages_nodes[dim_i].push({
            index: dimensions_pages_nodes[dim_i].length,
            x: computeX(thetaScale(dim_i)),
            y: computeY(thetaScale(dim_i)),
            url_hash: 0,
            cluster_index: 0,
            fake: true
          });
        // create the fixed node
        dimensions_pages_nodes[dim_i].push({
          index: dimensions_pages_nodes[dim_i].length,
          x: computeX(thetaScale(dim_i)),
          y: computeY(thetaScale(dim_i)),
          fx: computeX(thetaScale(dim_i)),
          fy: computeY(thetaScale(dim_i)),
          cluster_index: dim_i
        });
        // create the links
        for (let i = 0; i < dimensions_pages_nodes[dim_i].length - 1; i++)
          dimensions_pages_links[dim_i].push({
            target: i,
            source: dimensions_pages_nodes[dim_i].length - 1,
            cluster_index: dimensions_pages_nodes[dim_i][i].cluster_index,
            fake:
              dimensions_pages_nodes[dim_i].length === 3 &&
              dimensions_pages_nodes[dim_i][i].hasOwnProperty("fake") &&
              dimensions_pages_nodes[dim_i][i].fake
          });

        // rendering
        dimensions_pages_links_render.push(
          root
            .selectAll("line.link.concept-link.link-group-" + dim_i)
            .data(dimensions_pages_links[dim_i])
            .enter()
            .append("line")
            .classed("link concept-link link-group-" + dim_i, true)
            .classed("fake", d => d.hasOwnProperty("fake") && d.fake)
            .style("stroke", d => {
              return config.colorScale(config.colorAccessor(d));
            })
        );

        dimensions_pages_nodes_render.push(
          root
            .selectAll("circle.dot.label-node-group-" + dim_i)
            .data(dimensions_pages_nodes[dim_i])
            .enter()
            .append("circle")
            .classed("dot label-node-group-" + dim_i, true)
            .classed("label-node", d => d.index === dimensions_pages_nodes[dim_i].length - 1)
            .classed("concept-page", d => d.index !== dimensions_pages_nodes[dim_i].length - 1)
            .classed("fake", d => d.hasOwnProperty("fake") && d.fake)
            .attrs({
              cx: function(d) {
                return d.x;
              },
              cy: function(d) {
                return d.y;
              },
              r: d => {
                return d.index === dimensions_pages_nodes[dim_i].length - 1 ? 6 : 4;
              }
            })
            .style("fill", d => {
              return config.colorScale(config.colorAccessor(d));
            })
            .on("mouseenter", d => {
              if (d.index !== dimensions_pages_nodes[dim_i].length - 1) {
                if (d.hasOwnProperty("fake") && d.fake) return;
                TooltipService.showTooltip(
                  d.url_hash,
                  config.el.offsetLeft - config.dotRadius * 2 + d.x,
                  config.el.offsetTop + d.y,
                  config.colorScale(config.colorAccessor(d)),
                  d.index + 1,
                  d.cluster_index
                );
              }
            })
            .on("mouseleave", d => {
              if (d.index !== dimensions_pages_nodes[dim_i].length - 1)
                TooltipService.hideTooltip();
            })
        );

        /// force
        force.push(
          d3
            .forceSimulation(dimensions_pages_nodes[dim_i])
            //.force("link", d3.forceLink(dimensions_pages_links[dim_i]).strength(3))
            .force(
              "center",
              d3.forceCenter(
                dimensions_pages_nodes[dim_i][dimensions_pages_nodes[dim_i].length - 1].fx,
                dimensions_pages_nodes[dim_i][dimensions_pages_nodes[dim_i].length - 1].fy
              )
            )
            .stop()
        );
        let opened = false;
        dimensions_pages_nodes_render[dim_i]
          .on("click", e => {
            // add click event only on the fixed node that is the last one of the array
            if (e.index !== dimensions_pages_nodes[dim_i].length - 1) {
              if (e.hasOwnProperty("fake") && e.fake) return;
              setLoading(config.el, true);
              CacheService.getDocumentFromHash(e.url_hash).then(page => {
                setLoading(config.el, false);
                window.location.href = page.url;
              });
              return;
            }
            // add click event only if we have at least one concept page
            if (dimensions_pages_nodes[dim_i].length <= 1) return;

            if (!opened) {
              force[dim_i]
                .force(
                  "charge",
                  d3
                    .forceManyBody()
                    .strength(dimensions_pages_nodes[dim_i].length > 2 ? -100 : -400)
                    .distanceMax(200)
                )
                .force(
                  "link",
                  d3
                    .forceLink(dimensions_pages_links[dim_i])
                    .strength(dimensions_pages_nodes[dim_i].length > 2 ? 0.8 : 0.8)
                )
                .alpha(0.2)
                .restart();

              if (scope.opened_concepts === 0) dimEntirePanel(scope, config.el);
              scope.opened_concepts++;
            } else {
              force[dim_i]
                .force("charge", null)
                .force(
                  "link",
                  d3
                    .forceLink(dimensions_pages_links[dim_i])
                    .strength(1.5)
                    .distance(0)
                )
                .alpha(0.2)
                .restart();

              if (scope.opened_concepts === 1) undimEntirePanel(scope, config.el);
              scope.opened_concepts--;
            }
            opened = !opened;
          })
          .on("contextmenu", d3.contextMenu(scope.context_menu));

        force[dim_i].on("tick", _ => {
          dimensions_pages_links_render[dim_i].attrs({
            x1: function(d) {
              return d.source.x;
            },
            y1: function(d) {
              return d.source.y;
            },
            x2: function(d) {
              return d.target.x;
            },
            y2: function(d) {
              return d.target.y;
            }
          });

          dimensions_pages_nodes_render[dim_i].attrs({
            cx: function(d) {
              return d.x;
            },
            cy: function(d) {
              return d.y;
            }
          });
        });
      }
    };

    /**
     * Call this function when the user selects to remove a cluster
     * @param {*} cluster_index
     */
    let removeCluster = (scope, cluster_index) => {
      EventService.broadcast(EventService.events.REMOVE_CLUSTER, cluster_index);
    };

    ///// ui

    /**
     * Hover on a page node element on the graph
     * @param {*} scope
     * @param {*} chart_area
     * @param {*} hash
     * @param {*} dot
     */
    let hoverEnter = (scope, chart_area, hash, dot) => {
      let svg = d3.select(chart_area).select("svg");
      scope.currently_hover = hash;
      if (dot) dot.classList.add("selected");
      else svg.select(".dot-" + hash).classed("selected", true);
      svg.selectAll("circle.dot.chart-page").classed("dimmed", el => {
        return hash !== el.url_hash;
      });
      // links
      svg.selectAll(".link.chart-link").classed("dimmed", el => {
        return el.source.url_hash != hash;
      });
      svg.selectAll(".link.from-" + hash).classed("selected", true);
    };

    /**
     * Hover leave from an element on the graph
     * @param {*} scope
     * @param {*} chart_area
     * @param {*} hash
     * @param {*} dot
     */
    let hoverLeave = (scope, chart_area, hash, dot) => {
      let svg = d3.select(chart_area).select("svg");
      scope.currently_hover = undefined;
      if (dot) dot.classList.remove("selected");
      else svg.select(".dot-" + hash).classed("selected", false);
      // if no concept is opened
      if (scope.opened_concepts === 0)
        svg.selectAll("circle.dot.chart-page").classed("dimmed", false);
      else svg.select(".dot-" + hash).classed("dimmed", true);
      // links
      svg.selectAll(".link.from-" + hash).classed("selected", false);
      if (scope.opened_concepts === 0) svg.selectAll(".link.chart-link").classed("dimmed", false);
    };

    let selectPages = (scope, chart_area, pages_hashes) => {
      let svg = d3.select(chart_area).select("svg");
      let all_dots = svg.selectAll(".dot");
      // deselect all pages
      if (pages_hashes === null || pages_hashes.length === 0) {
        all_dots.classed("dimmed", false);
        all_dots.classed("selected", false);
        //svg.selectAll(".link.chart-link").classed("dimmed", false);
        //svg.selectAll(".link").classed("selected", false);
        return;
      }
      all_dots.classed("dimmed", true);
      pages_hashes.forEach((hash, i) => {
        // nodes
        svg
          .select(".dot-" + hash)
          .classed("selected", true)
          .classed("dimmed", false);
        // links
        /*
        svg.selectAll(".link.chart-link").classed("dimmed", el => {
          return el.source.url_hash != hash;
        });
        svg.selectAll(".link.from-" + hash).classed("selected", d => {
          return el.source.index === el.cluster_index + scope.concept_data.data.length;
        });
        */
      });
    };

    /**
     * Dim the entire chart, except for the concept nodes
     * @param {} _
     */
    let dimEntirePanel = (scope, chart_area) => {
      if (scope.circle_dimmed) return;
      scope.circle_dimmed = true;
      let svg = d3.select(chart_area);
      svg.select(".panel").classed("dimmed", true);
      svg.selectAll(".dot.chart-page").classed("dimmed", true);
      svg.selectAll(".link.chart-link").classed("dimmed", true);
      svg.selectAll("text.label").classed("dimmed", true);
    };

    let undimEntirePanel = (scope, chart_area) => {
      if (!scope.circle_dimmed) return;
      scope.circle_dimmed = false;
      let svg = d3.select(chart_area);
      svg.select(".panel").classed("dimmed", false);
      svg.selectAll(".dot.chart-page").classed("dimmed", false);
      svg.selectAll(".link.chart-link").classed("dimmed", false);
      svg.selectAll("text.label").classed("dimmed", false);
    };

    let setLoading = (chart_area, b) => {
      if (b) chart_area.classList.add("loading");
      else chart_area.classList.remove("loading");
    };

    return directiveDefinitionObject;
  }
})();
