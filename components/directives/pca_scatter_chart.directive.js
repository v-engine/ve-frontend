(function() {
  //"use strict";

  /**
   * D3 example
   * @ngdoc  Directive
   *
   * @example
   * <d3-test></d3-test>
   *
   */
  angular
    .module("boilerplate")
    .directive("pcaScatterChart", [
      "$log",
      "EventService",
      "CacheService",
      "GlobalVariablesService",
      "TooltipService",
      ScatterChart
    ]);

  function ScatterChart($log, EventService, CacheService, GlobalVariablesService, TooltipService) {
    let CHART_NAME = "ScatterChart";

    // Definition of directive
    let directiveDefinitionObject = {
      restrict: "E",
      replace: true,
      templateUrl: "components/directives/pca_scatter_chart.html",
      scope: { name: "@name", api_event: "@apiEvent", data: "=chartData" },
      controller: [
        "$scope",
        function PCAScatterChartController($scope) {
          $scope.currently_hover = undefined;
          $scope.brush_area_context_menu = [
            {
              title: "Query refinement"
            },
            {
              title: _ =>
                "Mark " + $scope.brushed_pages.length + " pages as non-relevant and re-run search",
              disabled: d => !$scope.brush_mode,
              action: (e, d, i) => {
                $scope.brush_mode = false;
                removePages($scope.brushed_pages);
              }
            }
          ];
          $scope.on_brush = false; // used for deciding if tooltip should be displayed
          $scope.brush_mode = false; // enable or disable the hover event
          $scope.brushed_pages = []; // array of url hashes of brushed pages
        }
      ],
      link: function(scope, element, attrs) {
        /// variables
        let chart_area = element[0];
        GlobalVariablesService.prepareChartArea(chart_area);
        let loading_events = [
          EventService.api_calls[scope.api_event],
          EventService.api_calls.QUERY,
          EventService.api_calls.REMOVE_CLUSTER,
          EventService.api_calls.REMOVE_PAGES
        ];
        /// listeners
        scope.$on(EventService.events.HOVER_ENTER, (event, data) => {
          if (data != scope.currently_hover) hoverEnter(scope, chart_area, data, null);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.HOVER_LEAVE, (event, data) => {
          hoverLeave(scope, chart_area, data, null);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.SELECTED_PAGES, (event, data) => {
          selectPages(scope, chart_area, data);
          scope.brush_mode = data.length === 0 ? false : true;
          scope.brushed_pages = data.map(d => d.cluster_index);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.RESIZE_END, (event, data) => {
          updateGraph(scope, chart_area);
          $log.debug(CHART_NAME + ": Received " + event.name);
        });
        scope.$on(EventService.events.API_REQUEST_START, (event, data) => {
          if (loading_events.indexOf(data) >= 0) {
            setLoading(chart_area, true);
            $log.debug(CHART_NAME + ": Received " + event.name);
          }
        });
        // watch the data variable and update the graph when it changes
        scope.$watch("data", function(new_value) {
          if (new_value != undefined) updateGraph(scope, chart_area);
        });
      }
    };

    /**
     * Update graph routine
     * From https://bl.ocks.org/mbostock/4062045
     * @param {*} scope
     */
    let updateGraph = function(scope, chart_area) {
      $log.debug(CHART_NAME + ": Update graph called, data is " + scope.data);
      // check if data is present
      if (scope.data === undefined) return;

      var width = chart_area.clientWidth,
        height = chart_area.clientHeight;

      var margin = { top: 30, right: 50, bottom: 40, left: 60 },
        width = width - margin.left - margin.right,
        height = height - margin.top - margin.bottom,
        circle_radius = 3.5;

      var x = d3.scaleLinear().range([0, width]);
      var y = d3.scaleLinear().range([height, 0]);

      var color = GlobalVariablesService.getColorScale(scope.data.n_clusters);

      var xAxis = d3.axisBottom(x);
      var yAxis = d3.axisLeft(y);

      d3.select(chart_area)
        .selectAll("svg")
        .remove();

      var svg = d3
        .select(chart_area)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      x.domain(
        d3.extent(scope.data.data, function(d) {
          return d.coordinates[0];
        })
      ).nice();
      y.domain(
        d3.extent(scope.data.data, function(d) {
          return d.coordinates[1];
        })
      ).nice();

      svg
        .append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .append("text")
        .attr("class", "label")
        .attr("x", width)
        .attr("y", -6)
        .style("text-anchor", "end")
        .text("x");

      svg
        .append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("y");

      let brush = svg
        .append("g")
        .attr("class", "brush")
        .call(
          d3
            .brush()
            .on("brush", _ => {
              d3.contextMenu("close");
              scope.on_brush = true;
            })
            .on("end", brushEnded)
        )
        .on("contextmenu", d3.contextMenu(scope.brush_area_context_menu))
        .on("click", _ => {
          d3.contextMenu("close");
        });

      let dots = svg
        .append("g")
        .attr("class", "dots")
        .selectAll(".dot")
        .data(scope.data.data)
        .enter()
        .append("circle")
        .attr("class", d => {
          return "dot dot-" + d.url_hash;
        })
        .attr("r", circle_radius)
        .attr("cx", function(d) {
          return x(d.coordinates[0]);
        })
        .attr("cy", function(d) {
          return y(d.coordinates[1]);
        })
        .style("fill", function(d) {
          return color(d.cluster_index);
        })
        .on("mouseenter", function(d) {
          if (scope.on_brush) return;
          let circle = d3.select(this);
          if (!scope.brush_mode) {
            EventService.broadcast(EventService.events.HOVER_ENTER, d.url_hash);
            hoverEnter(scope, chart_area, d.url_hash, circle);
          }
          TooltipService.showTooltip(
            d.url_hash,
            chart_area.offsetLeft + margin.left + circle_radius * 2 + parseInt(circle.attr("cx")),
            chart_area.offsetTop + margin.top + parseInt(circle.attr("cy")),
            color(d.cluster_index),
            d.index + 1,
            d.cluster_index
          );
        })
        .on("mouseleave", function(d, i) {
          if (scope.on_brush) return;
          if (!scope.brush_mode) {
            EventService.broadcast(EventService.events.HOVER_LEAVE, d.url_hash);
            let circle = d3.select(this);
            hoverLeave(scope, chart_area, d.url_hash, circle);
          }
          TooltipService.hideTooltip();
        })
        .on("click", (d, i) => {
          d3.contextMenu("close");
          setLoading(chart_area, true);
          CacheService.getDocumentFromHash(d.url_hash).then(page => {
            setLoading(chart_area, false);
            window.location.href = page.url;
          });
        });

      var legend = svg
        .selectAll(".legend")
        .data(color.domain())
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) {
          return "translate(0," + i * 20 + ")";
        });

      legend
        .append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

      legend
        .append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) {
          return d;
        });
      setLoading(chart_area, false);

      function brushEnded() {
        if (d3.event.selection != null) {
          let selected_pages_hashes = [];
          //console.log(d3.event.selection);
          var brush_coords = d3.brushSelection(this);
          // style brushed circles
          dots.filter(function(d) {
            var cx = d3.select(this).attr("cx"),
              cy = d3.select(this).attr("cy");
            let b = isBrushed(brush_coords, cx, cy);
            if (b) selected_pages_hashes.push(d.url_hash);
            return b;
          });
          // notify event to others
          EventService.broadcast(EventService.events.SELECTED_PAGES, selected_pages_hashes);
          scope.on_brush = false;
        }
      }
    };

    /*
     * UI
     */

    /**
     * Handle the hover on a dot
     * @param {*} scope
     * @param {*} svg
     * @param {*} hash
     * @param {Object} circle The circle, if available
     */
    let hoverEnter = (scope, chart_area, hash, circle) => {
      let svg = d3.select(chart_area).select("svg");
      scope.currently_hover = hash;
      // if we have _this the user hovered on the graph
      if (circle) circle.classed("selected", true);
      else {
        // otherwise hover event has been broadcasted
        svg.select(".dot-" + hash).classed("selected", true);
      }
      // update all circles
      svg.selectAll(".dot").classed("dimmed", el => {
        return el.url_hash !== hash;
      });
    };

    /**
     * Leave from the hover
     * @param {*} scope
     * @param {*} chart_area
     * @param {*} hash
     * @param {*} circle
     */
    let hoverLeave = (scope, chart_area, hash, circle) => {
      let svg = d3.select(chart_area).select("svg");
      scope.currently_hover = undefined;
      if (circle) circle.classed("selected", false);
      else svg.select(".dot-" + hash).classed("selected", false);
      svg.selectAll(".dot").classed("dimmed", false);
    };

    /**
     * Highlight pages in current chart
     * @param {*} scope
     * @param {*} pages_hashes
     * @param {*} dots d3 selection of dots
     */
    let selectPages = (scope, chart_area, pages_hashes) => {
      let svg = d3.select(chart_area).select("svg");
      let all_dots = svg.selectAll(".dot");
      // deselect all pages
      if (pages_hashes === null || pages_hashes.length === 0) {
        all_dots.classed("dimmed", false);
        all_dots.classed("selected", false);
        return;
      }
      all_dots.classed("dimmed", true);
      pages_hashes.forEach((hash, i) => {
        svg
          .select(".dot-" + hash)
          .classed("selected", true)
          .classed("dimmed", false);
      });
    };

    /**
     * Set the loading mode in the current chart
     * @param {*} chart_area
     * @param {*} b
     */
    let setLoading = (chart_area, b) => {
      if (b) chart_area.classList.add("loading");
      else chart_area.classList.remove("loading");
    };

    /*
     * Utils
     */

    /**
     * Remove the passed pages for query refinement
     * @param {*} pages
     */
    let removePages = pages => {
      EventService.broadcast(EventService.events.REMOVE_CLUSTER, pages);
    };

    /**
     * Check if passed current coordinates are in the brush area
     * @param {*} brush_coords
     * @param {*} cx
     * @param {*} cy
     */
    let isBrushed = (brush_coords, cx, cy) => {
      let x0 = brush_coords[0][0],
        x1 = brush_coords[1][0],
        y0 = brush_coords[0][1],
        y1 = brush_coords[1][1];

      return x0 <= cx && cx <= x1 && y0 <= cy && cy <= y1;
    };

    return directiveDefinitionObject;
  }
})();
