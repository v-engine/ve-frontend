// put extra scripts here

"use strict";

// Copies a variable number of methods from source to target.
d3.rebind = function(target, source) {
  var i = 1,
    n = arguments.length,
    method;
  while (++i < n) target[(method = arguments[i])] = d3_rebind(target, source, source[method]);
  return target;
};

// Method is assumed to be a standard D3 getter-setter:
// If passed with no arguments, gets the value.
// If passed with arguments, sets the value and returns the target.
function d3_rebind(target, source, method) {
  return function() {
    var value = method.apply(source, arguments);
    return value === source ? target : value;
  };
}

// scroll listener
window.addEventListener("scroll", e => {
  //let radar_chart = document.querySelector(".radar-chart");
  //let radar_chart_jq = angular.element(document.querySelector(".radar-chart"));
  let charts_area = document.querySelector(".charts");
  //let results_section = document.querySelector("section.results");
  let search_bar = document.querySelector("section.search");

  if (window.scrollY >= charts_area.offsetTop + charts_area.offsetHeight)
    search_bar.classList.add("slim");
  else search_bar.classList.remove("slim");
});
