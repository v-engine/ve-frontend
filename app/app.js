/**
 *
 * AngularJS Boilerplate
 * @description           Description
 * @author                Jozef Butko // www.jozefbutko.com/resume
 * @url                   www.jozefbutko.com
 * @version               1.1.7
 * @date                  March 2015
 * @license               MIT
 *
 */
(function() {
  /**
   * Definition of the main app module and its dependencies
   */
  angular.module("boilerplate", ["ngRoute"]).config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = [
    "$routeProvider",
    "$locationProvider",
    "$httpProvider",
    "$compileProvider",
    "$logProvider"
  ];

  /**
   * App routing
   *
   * You can leave it here in the config section or take it out
   * into separate file
   *
   */
  function config(
    $routeProvider,
    $locationProvider,
    $httpProvider,
    $compileProvider,
    $logProvider
  ) {
    // enable html5 mode and fix the hashbang
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix("");

    // routes
    $routeProvider
      .when("/", {
        templateUrl: "views/home.html",
        controller: "MainController",
        controllerAs: "main",
        reloadOnUrl: false
      })
      .when("/q/:q", {
        templateUrl: "views/home.html",
        controller: "SearchController",
        controllerAs: "search",
        reloadOnUrl: false
      })
      .when("/d3_test", {
        templateUrl: "views/d3_test.html",
        controller: "MainController",
        controllerAs: "main"
      })
      .when("/query", {
        templateUrl: "views/query.html",
        controller: "MainController",
        controllerAs: "main"
      })
      .otherwise({
        redirectTo: "/"
      });

    // enable logging
    $logProvider.debugEnabled(true);
    //$httpProvider.interceptors.push("authInterceptor");
  }

  /**
   * You can intercept any request or response inside authInterceptor
   * or handle what should happend on 40x, 50x errors
   *
   */
  angular.module("boilerplate").factory("authInterceptor", authInterceptor);

  authInterceptor.$inject = ["$rootScope", "$q", "LocalStorage", "$location"];

  function authInterceptor($rootScope, $q, LocalStorage, $location) {
    return {
      // intercept every request
      request: function(config) {
        config.headers = config.headers || {};
        return config;
      },

      // Catch 404 errors
      responseError: function(response) {
        if (response.status === 404) {
          $location.path("/");
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  }

  /**
   * Run block
   */
  angular.module("boilerplate").run(run);

  run.$inject = ["$rootScope", "$location", "$log"];

  function run($rootScope, $location, $log) {
    // put here everything that you need to run on page load
  }
})();
