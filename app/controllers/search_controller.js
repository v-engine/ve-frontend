/**
 * Search controller
 */
(function() {
  angular.module("boilerplate").controller("SearchController", SearchController);

  SearchController.$inject = [
    "$scope",
    "$log",
    "$routeParams",
    "$window",
    "$location",
    "GlobalVariablesService",
    "QueryService",
    "EventService",
    "DataGetterService"
  ];

  function SearchController(
    $scope,
    $log,
    $routeParams,
    $window,
    $location,
    GlobalVariablesService,
    QueryService,
    EventService,
    DataGetterService
  ) {
    CTRL_NAME = "SearchController";
    $log.debug(CTRL_NAME + " loaded");
    /// variables
    let self = this;
    $scope.query_input = undefined; // user input for the query
    $scope.query_results = undefined; // results of the query
    $scope.query_id = undefined; // the query_id given by the backend for further requests
    // charts
    $scope.web_graph_chart_data = undefined;
    $scope.concepts_chart_data = undefined;
    $scope.pca_scatter_chart_data = undefined;
    $scope.tsne_scatter_chart_data = undefined;
    $scope.radar_chart_data = undefined;

    /// listeners
    // listen here for query input changes, query input is changed by the checker below
    // or by the user when is searches for something
    $scope.$watch("query_input", function(value) {
      // skip false updates
      if (value !== undefined && value !== "") {
        // update the url
        $location.path("/q/" + $window.encodeURIComponent($scope.query_input));
        // perform the search
        self.search();
        $log.debug(CTRL_NAME + ': searched "' + $scope.query_input + '"');
      }
    });

    /// events
    $scope.$on(EventService.events.REMOVE_CLUSTER, (event, data) => {
      $log.debug(CTRL_NAME + ": Received event " + event.name);
      self.removeCluster(data);
    });

    $scope.$on(EventService.events.REMOVE_PAGES, (event, data) => {
      $log.debug(CTRL_NAME + ": Received event " + event.name);
      self.removePages(data);
    });

    // check if query is already in the address bar, only at first loading
    if ($routeParams.hasOwnProperty("q")) {
      $scope.query_input = $window.decodeURIComponent($routeParams["q"]);
      $log.debug(CTRL_NAME + ': Loading from url query given "' + $scope.query_input + '"');
    }

    /// functions
    /**
     * Handle the actual search and update the query_results controller variable
     */
    self.search = () => {
      EventService.broadcast(EventService.events.API_REQUEST_START, EventService.api_calls.QUERY);
      $log.debug(CTRL_NAME + ": Called function search");
      QueryService.query("GET", "query", { q: $scope.query_input }, {}).then(function(response) {
        if (response.status === 200) {
          $log.info('GET /query success for "' + $scope.query_input + '"');
          $scope.query_results = response.data.data;
          $scope.query_id = response.data.hash;
          GlobalVariablesService.setQueryId(response.data.hash);
          GlobalVariablesService.clearSelectedForRadar();
          self.loadWebGraphChartData();
        } else {
          $log.error(CTRL_NAME + ": GET /query error - " + JSON.stringify(response));
        }
      });
    };

    /**
     * Refine the query by selecting the passed cluster index as non relevant
     * @param {*} cluster_index
     */
    self.removeCluster = cluster_index => {
      EventService.broadcast(
        EventService.events.API_REQUEST_START,
        EventService.api_calls.REMOVE_CLUSTER
      );
      QueryService.query(
        "POST",
        "query_feedback/remove_cluster",
        {},
        { query_hash: $scope.query_id, cluster_index: cluster_index }
      ).then(function(response) {
        if (response.status === 200) {
          $log.info("POST /query_feedback/remove_cluster success");
          $scope.query_id = response.data.hash;
          $scope.query_results = response.data.data;
          GlobalVariablesService.setQueryId(response.data.hash);
          GlobalVariablesService.clearSelectedForRadar();
          self.loadWebGraphChartData();
        } else {
          $log.error("POST /query_feedback/remove_cluster error");
        }
      });
    };

    /**
     * Refine the query by selecting the passed pages as non relevant
     * @param {*} pages
     */
    self.removePages = pages => {
      EventService.broadcast(
        EventService.events.API_REQUEST_START,
        EventService.api_calls.REMOVE_CLUSTER
      );
      QueryService.query(
        "POST",
        "query_feedback/remove_pages",
        {},
        { query_hash: $scope.query_id, pages: pages }
      ).then(function(response) {
        if (response.status === 200) {
          $log.info("POST /query_feedback/remove_pages success");
          $scope.query_id = response.data.hash;
          $scope.query_results = response.data.data;
          GlobalVariablesService.setQueryId(response.data.hash);
          GlobalVariablesService.clearSelectedForRadar();
          self.loadWebGraphChartData();
        } else {
          $log.error("POST /query_feedback/remove_pages error");
        }
      });
    };

    /**
     * Load some data
     * @return {Object} Returned object
     */
    self.loadData = () => {
      QueryService.query("GET", "d3_test", {}, {}).then(function(response) {
        $log.debug("GET /d3_test success");
        $scope.web_graph_chart_data = response.data;
        $scope.concepts_chart_data = response.data;
      });
    };

    /**
     * Load web graph data
     * @return {Object} Returned object
     */
    self.loadWebGraphChartData = () => {
      DataGetterService.getWebGraphData($scope.query_id).then(web_graph_data => {
        //console.log(web_graph_data);
        $scope.web_graph_chart_data = web_graph_data;
        self.loadConceptsChartData();
      });
    };

    /**
     * Load concept graph data
     * @return {Object} Returned object
     */
    self.loadConceptsChartData = () => {
      DataGetterService.getConceptGraphData($scope.query_id).then(concept_data => {
        $scope.concepts_chart_data = concept_data;
        self.loadPCAScatterChartData();
        self.loadTSNEScatterChartData();
      });
    };

    /**
     * Load concept graph data
     * @return {Object} Returned object
     */
    self.loadPCAScatterChartData = () => {
      EventService.broadcast(
        EventService.events.API_REQUEST_START,
        EventService.api_calls.PCA_SCATTER_CHART
      );
      QueryService.query("GET", "pca_graph/" + $scope.query_id, {}, {}).then(function(response) {
        if (response.status === 200) {
          $log.info("GET /pca_graph/" + $scope.query_id + " success");
          $scope.pca_scatter_chart_data = response.data;
        } else {
          $log.error("GET /pca_graph/" + $scope.query_id + " error");
        }
      });
    };

    /**
     * Load concept graph data
     * @return {Object} Returned object
     */
    self.loadTSNEScatterChartData = () => {
      EventService.broadcast(
        EventService.events.API_REQUEST_START,
        EventService.api_calls.TSNE_SCATTER_CHART
      );
      QueryService.query("GET", "tsne_graph/" + $scope.query_id, {}, {}).then(function(response) {
        if (response.status === 200) {
          $log.info("GET /tsne_graph/" + $scope.query_id + " success");
          $scope.tsne_scatter_chart_data = response.data;
        } else {
          $log.error("GET /tsne_graph/" + $scope.query_id + " error");
        }
      });
    };
  }
})();
