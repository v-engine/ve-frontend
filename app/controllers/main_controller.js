/**
 * Main application controller
 */
(function() {
  angular.module("boilerplate").controller("MainController", MainController);

  MainController.$inject = [
    "$scope",
    "$log",
    "$routeParams",
    "$window",
    "$location",
    "LocalStorage",
    "QueryService",
    "DataGetterService"
  ];

  function MainController(
    $scope,
    $log,
    $routeParams,
    $window,
    $location,
    LocalStorage,
    QueryService,
    DataGetterService
  ) {
    $log.debug("MainController loaded");
    let self = this;
    /// variables
    $scope.query_input = undefined;

    /// listeners
    // listen here for query input changes, when the user click search the variable
    // is update accordingly by the search component
    $scope.$watch("query_input", function(value) {
      // skip false updates
      if (value !== undefined && value !== "") {
        // change the url with the query so that the SearchController is instantiated
        $location.path("/q/" + $window.encodeURIComponent($scope.query_input));
        $log.debug('MainController: searched "' + $scope.query_input + '"');
      }
    });

    $scope.loadWaterfall = () => {
      DataGetterService.getConceptGraphData(
        "aa97302150fce811425cd84537028a5afbe37e3f1362ad45a51d467e17afdc9c"
      ).then(response => {
        $log.debug("MainController: waterfall ready " + response);
      });
    };
  }
})();
