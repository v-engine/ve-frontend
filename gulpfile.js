/**
 * @author  Jozef Butko
 * @url		  www.jozefbutko.com/resume
 * @date    March 2015
 * @license MIT
 *
 * AngularJS Boilerplate: Build, watch and other useful tasks
 *
 * The build process consists of following steps:
 * 1. clean /_build folder
 * 2. compile SASS files, minify and uncss compiled css
 * 3. copy and minimize images
 * 4. minify and copy all HTML files into $templateCache
 * 5. build index.html
 * 6. minify and copy all JS files
 * 7. copy fonts
 * 8. show build folder size
 *
 */
const gulp = require("gulp");
const fs = require("fs");
const browserSync = require("browser-sync");
const reload = browserSync.reload;
const $ = require("gulp-load-plugins")();
const del = require("del");
const googleWebFonts = require("gulp-google-webfonts");
const postcss_uncss = require("postcss-uncss");
const path = require("path");
const url = require("url");

// optimize images
gulp.task("images", function(done) {
  return gulp
    .src("./images/**/*")
    .pipe($.changed("./_build/images"))
    .pipe(
      $.imagemin({
        optimizationLevel: 3,
        progressive: true,
        interlaced: true
      })
    )
    .pipe(gulp.dest("./_build/images"));
});

// The default file if the file/path is not found
var defaultFile = "index.html";
var folder = path.resolve(__dirname);
// browser-sync task, only cares about compiled CSS
gulp.task("browser-sync", function(done) {
  return browserSync({
    server: {
      baseDir: "./",
      middleware: function(req, res, next) {
        var fileName = url.parse(req.url);
        fileName = fileName.href.split(fileName.search).join("");
        var fileExists = fs.existsSync(folder + fileName);
        if (!fileExists && fileName.indexOf("browser-sync-client") < 0) {
          req.url = "/" + defaultFile;
        }
        return next();
      }
    }
  });
});

// minify JS
gulp.task("minify-js", function(done) {
  return gulp
    .src("js/*.js")
    .pipe($.uglify())
    .pipe(gulp.dest("./_build/"));
});

// minify CSS
gulp.task("minify-css", function(done) {
  return gulp
    .src(["./styles/**/*.css", "!./styles/**/*.min.css"])
    .pipe($.rename({ suffix: ".min" }))
    .pipe($.minifyCss({ keepBreaks: true }))
    .pipe(gulp.dest("./styles/"))
    .pipe(gulp.dest("./_build/css/"));
});

// minify HTML
gulp.task("minify-html", function(done) {
  var opts = {
    comments: true,
    spare: true,
    conditionals: true
  };

  return gulp
    .src("./*.html")
    .pipe($.minifyHtml(opts))
    .pipe(gulp.dest("./_build/"));
});

/*
 * Fonts
 */

gulp.task("fonts-fa-wf", function(done) {
  // copy font awesome fonts for build
  return gulp
    .src("./node_modules/@fortawesome/fontawesome-free/webfonts/*.*")
    .pipe(gulp.dest("./styles/fonts"));
});

gulp.task("fonts-fa-css", function(done) {
  // fix fonts import dir
  return (
    gulp
      .src(["./node_modules/@fortawesome/fontawesome-free/css/all.css"])
      .pipe($.replace("../webfonts/", "fonts/"))
      //.pipe($.minifyCss({ keepBreaks: true }))
      .pipe($.rename("font-awesome.css"))
      .pipe(gulp.dest("./styles/"))
  );
});

// download required fonts
gulp.task(
  "fonts-download",
  gulp.parallel("fonts-fa-wf", "fonts-fa-css", function(done) {
    // download google fonts
    // check if fonts.css already exists
    if (!fs.existsSync("./styles/fonts.css"))
      return gulp
        .src("./styles/fonts.list")
        .pipe(
          googleWebFonts({
            fontsDir: "./fonts",
            cssDir: "./"
          })
        )
        .pipe(gulp.dest("./styles"));
    else done();
  })
);

// copy fonts from a module outside of our project (like Bower)
gulp.task(
  "fonts",
  gulp.series("fonts-download", function(done) {
    return gulp
      .src("./styles/fonts/**/*.{ttf,woff,eof,eot,svg}")
      .pipe($.changed("./_build/css/fonts"))
      .pipe(gulp.dest("./_build/css/fonts"));
  })
);

// start webserver
gulp.task("server", function(done) {
  return browserSync(
    {
      server: {
        baseDir: "./"
      }
    },
    done
  );
});

// start webserver from _build folder to check how it will look in production
gulp.task("server-build", function(done) {
  return browserSync(
    {
      server: {
        baseDir: "./_build/"
      }
    },
    done
  );
});

// delete build folder
gulp.task("clean:build", function(cb) {
  del(
    [
      "./_build/",
      "./fonts/",
      "./styles/*.css",
      "./styles/fonts"
      // if we don't want to clean any file we can use negate pattern
      //'!dist/mobile/deploy.json'
    ],
    cb
  );
});

// concat files
gulp.task("concat", function(done) {
  return gulp
    .src("./js/*.js")
    .pipe($.concat("scripts.js"))
    .pipe(gulp.dest("./_build/"));
});

// SASS task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task("sass", function(done) {
  return gulp
    .src("styles/style.scss")
    .pipe($.sourcemaps.init())
    .pipe(
      $.sass({
        style: "expanded"
      })
    )
    .on(
      "error",
      $.notify.onError({
        title: "SASS Failed",
        message: "Error(s) occurred during compile!"
      })
    )
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest("styles"))
    .pipe(
      reload({
        stream: true
      })
    )
    .pipe(
      $.notify({
        message: "Styles task complete"
      })
    );
});

// SASS Build task
gulp.task("sass:build", function(done) {
  var s = $.size();

  return (
    gulp
      .src("styles/style.scss")
      .pipe(
        $.sass({
          style: "compact"
        })
      )
      .pipe($.autoprefixer("last 3 version"))
      //.pipe(
      //  $.postcss([
      //    postcss_uncss({
      //      html: ["./index.html", "./views/**/*.html", "./components/**/*.html"],
      //      ignore: [".index", ".slick", /\.owl+/, /\.owl-next/, /\.owl-prev/]
      //    })
      //  ])
      //)
      .pipe(
        $.minifyCss({
          keepBreaks: true,
          aggressiveMerging: false,
          advanced: false
        })
      )
      .pipe($.rename({ suffix: ".min" }))
      .pipe(gulp.dest("_build/css"))
      .pipe(s)
      .pipe(
        $.notify({
          onLast: true,
          message: function() {
            return "Total CSS size " + s.prettySize;
          }
        })
      )
  );
});

// BUGFIX: warning: possible EventEmitter memory leak detected. 11 listeners added.
require("events").EventEmitter.prototype._maxListeners = 100;

// index.html build
// script/css concatenation
gulp.task("usemin", function(done) {
  return (
    gulp
      .src("./index.html")
      // add templates path
      .pipe(
        $.htmlReplace({
          templates: '<script type="text/javascript" src="js/templates.js"></script>'
        })
      )
      .pipe(
        $.usemin({
          css: [$.minifyCss(), "concat"],
          libs: [
            /*$.uglify()*/
          ],
          nonangularlibs: [$.uglify()],
          angularlibs: [$.uglify()],
          appcomponents: [
            /*$.uglify()*/
          ],
          mainapp: [
            /*$.uglify()*/
          ]
        })
      )
      .pipe(gulp.dest("./_build/"))
  );
});

// make templateCache from all HTML files
gulp.task("templates", function(done) {
  return gulp
    .src(["./**/*.html", "!node_modules/**/*.*", "!_build/**/*.*"])
    .pipe($.minifyHtml())
    .pipe(
      $.angularTemplatecache({
        module: "boilerplate"
      })
    )
    .pipe(gulp.dest("_build/js"));
});

// reload all Browsers
gulp.task("bs-reload", function(done) {
  browserSync.reload();
  done();
});

// calculate build folder size
gulp.task("build:size", function(done) {
  var s = $.size();

  return gulp
    .src("./_build/**/*.*")
    .pipe(s)
    .pipe(
      $.notify({
        onLast: true,
        message: function() {
          return "Total build size " + s.prettySize;
        }
      })
    );
});

// default task to be run with `gulp` command
// this default task will run BrowserSync & then use Gulp to watch files.
// when a file is changed, an event is emitted to BrowserSync with the filepath.
gulp.task(
  "default",
  gulp.parallel("browser-sync", "fonts", "sass", "minify-css", function(done) {
    gulp.watch("styles/*.css", function(file) {
      if (file.type === "changed") {
        reload(file.path);
      }
    });
    gulp.watch(["*.html", "components/**/*.html", "views/*.html"], gulp.series("bs-reload"));
    gulp.watch(["app/**/*.js", "components/**/*.js", "js/*.js"], gulp.series("bs-reload"));
    gulp.watch("styles/**/*.scss", gulp.series("sass", "minify-css"));
    done();
  })
);

/**
 * build task:
 * 1. clean /_build folder
 * 2. compile SASS files, minify and uncss compiled css
 * 3. copy and minimize images
 * 4. minify and copy all HTML files into $templateCache
 * 5. build index.html
 * 6. minify and copy all JS files
 * 7. copy fonts
 * 8. show build folder size
 *
 */
gulp.task(
  "build",
  gulp.series(
    "clean:build",
    "fonts",
    "sass",
    "sass:build",
    "images",
    "templates",
    "usemin",
    "build:size",
    function(done) {
      done();
    }
  )
);
